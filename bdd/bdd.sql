-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: projet_poo_gestion_materiels
-- ------------------------------------------------------
-- Server version	5.6.28-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Admin`
--
DROP DATABASE IF EXISTS projet_poo_gestion_materiels;
CREATE DATABASE projet_poo_gestion_materiels;
USE projet_poo_gestion_materiels;

DROP TABLE IF EXISTS `Admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin` (
  `id` int(11) NOT NULL,
  `login` varchar(30) NOT NULL,
  `mdp` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admin`
--

LOCK TABLES `Admin` WRITE;
/*!40000 ALTER TABLE `Admin` DISABLE KEYS */;
INSERT INTO `Admin` VALUES (1,'rachdinou','passer');
/*!40000 ALTER TABLE `Admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Classe`
--

DROP TABLE IF EXISTS `Classe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Classe` (
  `nom` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Classe`
--

LOCK TABLES `Classe` WRITE;
/*!40000 ALTER TABLE `Classe` DISABLE KEYS */;
/*!40000 ALTER TABLE `Classe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Emprunt`
--

DROP TABLE IF EXISTS `Emprunt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Emprunt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_emprunt` date NOT NULL,
  `date_retour` date DEFAULT NULL,
  `rendu` tinyint(1) NOT NULL,
  `matr_resp` varchar(30) DEFAULT NULL,
  `motif` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_resp_emp` (`matr_resp`),
  CONSTRAINT `fk_resp_emp` FOREIGN KEY (`matr_resp`) REFERENCES `Responsable` (`matr`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Emprunt`
--

LOCK TABLES `Emprunt` WRITE;
/*!40000 ALTER TABLE `Emprunt` DISABLE KEYS */;
/*!40000 ALTER TABLE `Emprunt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Emprunt_Materiel`
--

DROP TABLE IF EXISTS `Emprunt_Materiel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Emprunt_Materiel` (
  `id_emp` int(11) NOT NULL DEFAULT '0',
  `id_materiel` int(11) NOT NULL DEFAULT '0',
  `id_emp_mat` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_emp_mat`,`id_emp`,`id_materiel`),
  KEY `fk_emp_emp_mat` (`id_emp`),
  KEY `fk_mat_emp_mat` (`id_materiel`),
  CONSTRAINT `Emprunt_Materiel_ibfk_1` FOREIGN KEY (`id_materiel`) REFERENCES `Materiel` (`id`),
  CONSTRAINT `fk_emp_emp_mat` FOREIGN KEY (`id_emp`) REFERENCES `Emprunt` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Emprunt_Materiel`
--

LOCK TABLES `Emprunt_Materiel` WRITE;
/*!40000 ALTER TABLE `Emprunt_Materiel` DISABLE KEYS */;
/*!40000 ALTER TABLE `Emprunt_Materiel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Gerant`
--

DROP TABLE IF EXISTS `Gerant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Gerant` (
  `matr` varchar(30) NOT NULL DEFAULT '',
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `login` varchar(30) NOT NULL,
  `mdp` varchar(30) NOT NULL,
  PRIMARY KEY (`matr`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Gerant`
--

LOCK TABLES `Gerant` WRITE;
/*!40000 ALTER TABLE `Gerant` DISABLE KEYS */;
INSERT INTO `Gerant` VALUES ('1','nou','rachdi','rachdinou','gerer');
/*!40000 ALTER TABLE `Gerant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Materiel`
--

DROP TABLE IF EXISTS `Materiel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Materiel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('Projecteur','Multiprise','Cable','PointAcces') DEFAULT NULL,
  `libelle` text,
  `date_livraison` date NOT NULL,
  `etat` enum('Disponible','Endommage','Emprunte','Panne','Panier') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1247 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Materiel`
--

LOCK TABLES `Materiel` WRITE;
/*!40000 ALTER TABLE `Materiel` DISABLE KEYS */;
/*!40000 ALTER TABLE `Materiel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Responsable`
--

DROP TABLE IF EXISTS `Responsable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Responsable` (
  `matr` varchar(30) NOT NULL DEFAULT '',
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `mail` varchar(30) NOT NULL,
  `tel` varchar(9) NOT NULL,
  `adresse` text,
  `date_naissance` date NOT NULL,
  `num_classe` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`matr`),
  UNIQUE KEY `mail` (`mail`),
  UNIQUE KEY `tel` (`tel`),
  KEY `fk_classe_resp` (`num_classe`),
  CONSTRAINT `fk_classe_resp` FOREIGN KEY (`num_classe`) REFERENCES `Classe` (`nom`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Responsable`
--

LOCK TABLES `Responsable` WRITE;
/*!40000 ALTER TABLE `Responsable` DISABLE KEYS */;
/*!40000 ALTER TABLE `Responsable` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-24 15:23:19
