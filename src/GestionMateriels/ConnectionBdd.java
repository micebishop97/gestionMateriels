package GestionMateriels;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.*;
import java.util.prefs.Preferences;
import java.util.Scanner;

/**
 *
 * @author micebishop
 */
public class ConnectionBdd {
    public static Connection conn;
    
    Preferences preferences = Preferences.userNodeForPackage(ConnectionBdd.class);
    private void setCredentials(String username, String password, String bdd){
        preferences.put("db_username", username);
        preferences.put("db_password", password);
        preferences.put("db_name", bdd);
    }
    
    private String getUsername(){
        return preferences.get("db_username", null);
    }
    
    private String getPassword(){
        return preferences.get("db_password", null);
    }
    
    private String getBdd(){
        return preferences.get("db_name", null);
    }
    
    public ConnectionBdd(){        
        Scanner sc = new Scanner(System.in);
        
        String url = "jdbc:mysql://localhost:3306/";
//        String username=null, password=null, bdd=null;        
//        
//        System.out.println("Enter your username ...");
//        username = sc.next();
//
//        System.out.println("Enter your password...");
//        password = sc.next();       
//        
//        System.out.println("Enter your database name ...");
//        bdd = sc.next();
//        
//        setCredentials(username, password, bdd);
        
        System.out.println("Connecting database...");
        try {
            conn = DriverManager.getConnection(url+getBdd(), getUsername(), getPassword());
            System.out.println("Database connected!");
        } catch (SQLException e) {
            throw new IllegalStateException("Cannot connect the database!", e);
        }
    }        
}
