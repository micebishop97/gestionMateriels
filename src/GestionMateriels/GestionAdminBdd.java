/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestionMateriels;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author micebishop
 */
public class GestionAdminBdd {
    
    /**
     *
     * @param login
     * @param mdp
     * @return Boolean
     * Verifie si l'utilisateur est l'admin
     */
    public static boolean isADmin(String login, String mdp){
        boolean isAdmin = false;
        try {
            Statement state = ConnectionBdd.conn.createStatement();
            ResultSet result = state.executeQuery("SELECT login, mdp FROM Admin WHERE login='" + login + "' AND mdp='" + mdp + "'");
            if (result.next())
                isAdmin = true;
                
        } catch (SQLException ex) {
            Logger.getLogger(GestionAdminBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isAdmin;
    } 
    
    public static boolean isGerant(String login, String mdp){
        boolean isGerant = false;
        try {
            Statement state = ConnectionBdd.conn.createStatement();
            ResultSet result = state.executeQuery("SELECT login, mdp FROM Gerant WHERE login='" + login + "' AND mdp='" + mdp + "'");
            if (result.next())
                isGerant = true;
                
        } catch (SQLException ex) {
            Logger.getLogger(GestionAdminBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isGerant;
    }
    
    
    // INACHEVE
//    public Responsable getResponsableByNum(String num){
//        Responsable resp = new Responsable();
//        try {
//            Statement state = ConnectionBdd.conn.createStatement();
//            ResultSet result = state.executeQuery("SELECT * FROM Responsable WHERE num="+num);
//            while(result.next()){
//                resp.setNom(result.getString("nom"));
//                resp.setPrenom(result.getString("prenom"));
//                resp.setAdresse(result.getString("adresse"));
//                resp.setDateNaissance(AllUtils.toJavaDate(result.getObject("date_naissance").toString()));
//                resp.setMail(result.getString("mail"));
//                resp.setTel(result.getString("tel"));
//                Classe classe = new Classe();
//                classe.setNum(result.getString("num_classe"));
//                resp.setClasse(classe);
//                classe.setResponsable(resp);
//                
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(GestionAdminBdd.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    
    // des fonctions qui nous permet d'ajouter/supprimer/ des classes; des responsables des gerants et des departements 
    public static boolean ajouterClasse(Classe classe){
        try {
            Statement state = ConnectionBdd.conn.createStatement();
            state.executeUpdate("INSERT INTO Classe (nom) values('" +classe.getNom()+"')");
                
        } catch (SQLException ex) {
            Logger.getLogger(GestionAdminBdd.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    ///suprimer une classe
    public static boolean supprimerClasse(String nom){
        try {
            Statement state = ConnectionBdd.conn.createStatement();
            state.executeUpdate("DELETE FROM Classe WHERE (nom='" +nom+"')");
                
        } catch (SQLException ex) {
            Logger.getLogger(GestionAdminBdd.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public static ArrayList<Classe> listeClasse(){
        ArrayList<Classe> liste = new ArrayList();
        
        try {            
            Statement state = ConnectionBdd.conn.createStatement();
            ResultSet result = state.executeQuery("SELECT nom FROM Classe");
            while(result.next()){
                Classe classe = new Classe(result.getString("nom"));
                liste.add(classe);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GestionAdminBdd.class.getName()).log(Level.SEVERE, null, ex);
        }            
        return liste;
    }
    //ajouter un responsable
    
    public static boolean ajouterResponsable(Responsable resp){
        try {
            Statement state = ConnectionBdd.conn.createStatement();
            state.executeUpdate("INSERT INTO Responsable (matr, nom,prenom,mail,tel,adresse,date_naissance,num_classe) values('" +resp.getMatr()+ "','"+resp.getNom()+"','"+resp.getPrenom()+"','"+resp.getMail()+"','"+resp.getTel()+"','"+resp.getAdresse()+"','"+AllUtils.toSqlDate(resp.getDateNaissance())+"','"+resp.getClasse().getNom()+"')");
            
        } catch (SQLException ex) {
            Logger.getLogger(GestionAdminBdd.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
        
    //supprimer un responsable
    public static boolean supprimerResponsable(String matr){
        try {
            Statement state = ConnectionBdd.conn.createStatement();
            state.executeUpdate("DELETE FROM Responsable WHERE (matr='" +matr+"')");
                
        } catch (SQLException ex) {
            Logger.getLogger(GestionAdminBdd.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
   // ajouter un gerant
//    public void ajouterGerant(Gerant gerant){
//        try {
//            Statement state = ConnectionBdd.conn.createStatement();
//            state.executeUpdate("INSERT INTO Gerant (matr,nom,prenom,login,mdp) values('" +gerant.getMatr()+ "','"+gerant.getNom()+"','"+gerant.getPrenom()+"','"+gerant.getLogin()+"','"+gerant.getMotDePasse()+"')");
//                
//        } catch (SQLException ex) {
//            Logger.getLogger(GestionAdminBdd.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    
    // supprimer un gerant
//    public void suprimerGerant(Gerant gerant){
//        try {
//            Statement state = ConnectionBdd.conn.createStatement();
//            state.executeUpdate("DELETE FROM Gerant WHERE (id='" +gerant.getMatr()+"')");
//                
//        } catch (SQLException ex) {
//            Logger.getLogger(GestionAdminBdd.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    
}
