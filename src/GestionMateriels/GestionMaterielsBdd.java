/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestionMateriels;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author micebishop
 */
public class GestionMaterielsBdd {

    /**
     *
     * @param mat
     * Ajoute un materiel dans la base de donnees
     */
    
    public static boolean ajoutMateriel(Materiel mat){
        try{
            Statement ajout_mat_state = ConnectionBdd.conn.createStatement();
            String typemat= mat.getType().toString();
            String libellemat = mat.getLibelle();
            Date dateLivraisonmat = mat.getDateLivraison();
            String etatmat = mat.getEtat().toString();
            
            ajout_mat_state.executeUpdate("insert into Materiel (type,libelle,date_livraison,etat) values ('"+ typemat + "','" + libellemat + "','" + AllUtils.toSqlDate(dateLivraisonmat) + "','" + etatmat + "')");
            Statement state = ConnectionBdd.conn.createStatement();
            ResultSet result = state.executeQuery("SELECT LAST_INSERT_ID()");
            result.next();
            mat.setId(result.getInt("LAST_INSERT_ID()"));
            
        }catch(SQLException ex){
            Logger.getLogger(GestionEmpruntsBdd.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;        
    }
    
    /**
     *
     * @param idmatsup
     * Enleve de la base de donne un materiel
     * @param etat
     */
    public static boolean changerEtatMateriel(String id, EtatMateriel etat){
        
        try{
            Statement state = ConnectionBdd.conn.createStatement();
            state.executeUpdate("UPDATE Materiel SET etat='"+etat+"' WHERE id='"+id+"'");
        }catch(SQLException ex){
            Logger.getLogger(GestionEmpruntsBdd.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
            
    } 
    
//    public static ArrayList<Materiel> listeMateriels(){
//        ArrayList<Materiel> liste;
//        liste = new ArrayList();
//        try{
//            Statement liste_mat_state = ConnectionBdd.conn.createStatement();
//            ResultSet liste_mat_result = liste_mat_state.executeQuery("select * from Materiel");
//            while (liste_mat_result.next()){
//                Materiel mat = new Materiel();
//                mat.setId(liste_mat_result.getString("id"));
//                String type = liste_mat_result.getString("type");
//                mat.setType(TypeMateriel.valueOf(type));
//                mat.setLibelle(liste_mat_result.getString("libelle"));
//                String date = liste_mat_result.getString("date_livraison");
//                mat.setDateLivraison(AllUtils.toJavaDate(date));
//                mat.setDepartement(new Departement());
//                mat.setDisponible(Boolean.valueOf(liste_mat_result.getObject("disponible").toString()));
//                liste.add(mat);
//                
//            }
//        }catch(SQLException ex){
//            Logger.getLogger(GestionEmpruntsBdd.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return liste;
//    }

    /**
     *
     * @param dispo
     * @return ArrayList of Materiel
     * Liste les Materiels selon le statut donne en parametre
     */
    
     public static ArrayList<Materiel> listeMateriels(EtatMateriel etat){
        ArrayList<Materiel> liste = new ArrayList();
        
        try{
            Statement dispo_mat_state = ConnectionBdd.conn.createStatement();
            ResultSet result = dispo_mat_state.executeQuery("select * from Materiel where etat='"+etat.toString()+"'");
            
            while(result.next()){
                Materiel mat = new Materiel();
                mat.setId(result.getInt("id"));
                mat.setLibelle(result.getString("libelle"));
                mat.setEtat(EtatMateriel.valueOf(result.getString("etat")));
                //mat.setDisponible(disponible);
                mat.setDateLivraison(AllUtils.toJavaDate(result.getObject("date_livraison").toString()));
                mat.setType(TypeMateriel.valueOf(result.getString("type")));
                liste.add(mat);
            }
            
        }catch(SQLException ex){
            Logger.getLogger(GestionEmpruntsBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }
    
     public static ArrayList<Materiel> listeMateriels(EtatMateriel etat, TypeMateriel type){
        ArrayList<Materiel> liste = new ArrayList();
        
        try{
            Statement dispo_mat_state = ConnectionBdd.conn.createStatement();
            ResultSet result = dispo_mat_state.executeQuery("select * from Materiel where etat='"+etat.toString()+"' AND type='" + type.toString() + "'");
            
            while(result.next()){
                Materiel mat = new Materiel();
                mat.setId(result.getInt("id"));
                mat.setLibelle(result.getString("libelle"));
                mat.setEtat(EtatMateriel.valueOf(result.getString("etat")));
                //mat.setDisponible(disponible);
                mat.setDateLivraison(AllUtils.toJavaDate(result.getObject("date_livraison").toString()));
                mat.setType(TypeMateriel.valueOf(result.getString("type")));
                liste.add(mat);
            }
            
        }catch(SQLException ex){
            Logger.getLogger(GestionEmpruntsBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }
     
    public static Materiel getMaterielById(int id){
        Materiel materiel = new Materiel();
        try {
            Statement state = ConnectionBdd.conn.createStatement();
            ResultSet result = state.executeQuery("SELECT * FROM Materiel WHERE id="+id);
            
            if (result.next()){
                materiel.setId(id);
                materiel.setType(TypeMateriel.valueOf(result.getString("type")));
                materiel.setLibelle(result.getString("libelle"));
                materiel.setDateLivraison(AllUtils.toJavaDate(result.getString("date_livraison")));
                materiel.setEtat(EtatMateriel.valueOf(result.getString("etat")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(GestionMaterielsBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return materiel;
    }
    
    public static String montrerMaterielsEmp(int idEmp){
        String message = "";
        try {
            Statement state = ConnectionBdd.conn.createStatement();
            ResultSet result = state.executeQuery("SELECT Materiel.id, Materiel.type FROM Emprunt_Materiel, Emprunt, Materiel WHERE Emprunt_Materiel.id_emp = Emprunt.id AND id_emp=" + idEmp +" AND Materiel.id = Emprunt_Materiel.id_materiel");
            while(result.next()){
                message += result.getString("id") + "-" + result.getString("type") + "\n";
            }
        } catch (SQLException ex) {
            Logger.getLogger(GestionMaterielsBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return message;
    }
    
     public static DefaultTableModel retournerDefaultTableModel (EtatMateriel etat, TypeMateriel type){
        DefaultTableModel table = new DefaultTableModel();
        
        try{
            Statement dispo_mat_state = ConnectionBdd.conn.createStatement();
            ResultSet result;
            if (etat.equals(EtatMateriel.Tout) && !type.equals(TypeMateriel.Tout))
                result = dispo_mat_state.executeQuery("select * from Materiel where type='" + type.toString() + "'");
            else if (type.equals(TypeMateriel.Tout) && !etat.equals(EtatMateriel.Tout))
                result = dispo_mat_state.executeQuery("select * from Materiel where etat='"+etat.toString()+"'");
            else if (etat.equals(EtatMateriel.Tout) && type.equals(TypeMateriel.Tout))
                result = dispo_mat_state.executeQuery("select * from Materiel");
            else
                result = dispo_mat_state.executeQuery("select * from Materiel where etat='"+etat.toString()+"' AND type='" + type.toString() + "'");
                
            
            
//            while(result.next()){
//                Materiel mat = new Materiel();
//                mat.setId(result.getInt("id"));
//                mat.setLibelle(result.getString("libelle"));
//                mat.setEtat(EtatMateriel.valueOf(result.getString("etat")));
//                //mat.setDisponible(disponible);
//                mat.setDateLivraison(AllUtils.toJavaDate(result.getObject("date_livraison").toString()));
//                mat.setType(TypeMateriel.valueOf(result.getString("type")));
//                liste.add(mat);
//            }
       table = AllUtils.buildTableModel(result);
            
        }catch(SQLException ex){
            Logger.getLogger(GestionEmpruntsBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return table;
    }
    
}
