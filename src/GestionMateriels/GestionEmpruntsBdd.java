package GestionMateriels;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author micebishop
 */
public class GestionEmpruntsBdd {

    /**
     *
     * @param emprunt
     * Ajoute un EMprunt dans la base de donnees
     */
    public static boolean ajouterEmprunt(String matrResp, ArrayList<Materiel> listeMatEmp, String motif){
        try {
            Statement state = ConnectionBdd.conn.createStatement();
//            int rendu = 0;
//            if (emprunt.isRendu())
//                rendu = 1;
            //Emprunt emprunt = new Emprunt();
            state.executeUpdate("INSERT INTO Emprunt (date_emprunt, rendu, matr_resp, motif) VALUES('" +
                    AllUtils.toSqlDate(new Date()) +
                    "',0" +
                    ",'" + matrResp +
                    "','" + motif +
                    "')");
            ResultSet result = state.executeQuery("SELECT LAST_INSERT_ID()");
            int id = 0;
            if (result.next()){
                id = result.getInt(1);
                System.out.println("Emprunt " + id + " ajoutee");
            }
            
            int i;
            for (i=0; i<listeMatEmp.size(); i++){
                state.executeUpdate("INSERT INTO Emprunt_Materiel (id_emp, id_materiel) VALUES ('"+ id +
                    "','" + listeMatEmp.get(i).getId() +
                    "')");
                System.out.println("Materiel " + listeMatEmp.get(i).getId() +" ajoute dans l'emprunt");
                
                state.executeUpdate("UPDATE Materiel SET etat='Emprunte' WHERE id='"+listeMatEmp.get(i).getId()+"'");
            }
            
            
            
            
        } catch (SQLException ex) {
            Logger.getLogger(GestionEmpruntsBdd.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    /**
     *
     * @param resp
     * @param emp
     * Met a jour le statut de l'emprunt et le statut des materiels empruntes
     */
    public static boolean reglerEmprunt(String matrResp, int idEmp){
        try {
            Statement state = ConnectionBdd.conn.createStatement();
            state.executeUpdate("UPDATE Emprunt SET rendu=1, date_retour='" + AllUtils.toSqlDate(new Date()) + "' WHERE (id='" + idEmp +
                    "' AND matr_resp='" + matrResp +                    
                    "')");
            System.out.println("Emprunt no " + idEmp + "regle");
            
            int i;
            ArrayList listeMat = getIdMaterielsOfEmp(idEmp);
            for (i=0; i<listeMat.size(); i++){
                state.executeUpdate("UPDATE Materiel SET etat='Disponible' WHERE id='"+ listeMat.get(i) +
                        "'");
                System.out.println("Materiel no " + listeMat.get(i) + "desormais disponible");
            }
        } catch (SQLException ex) {
            Logger.getLogger(GestionEmpruntsBdd.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
       return true;
    }
    
    /**
     *
     * @param rendu
     * @return ArrayList de Materiel
     * Retourne la liste des Emprunts selon le statut donne en parametre
     */
    public static ArrayList<Emprunt> listerEmprunts(boolean rendu){
        ArrayList<Emprunt> liste = new ArrayList();
        try {            
            Statement state = ConnectionBdd.conn.createStatement();
            ResultSet result;
            if (rendu)
                result = state.executeQuery("SELECT num, num_resp, date_emprunt, date_retour  FROM Emprunt WHERE rendu=1");
            else
                result = state.executeQuery("SELECT num, num_resp, date_emprunt, date_retour  FROM Emprunt WHERE rendu=0");
//            Emprunt emp = new Emprunt(num, resp, dateEmprunt, true)
            while (result.next()){
                String num = result.getString("num");
                String num_resp = result.getString("num_resp");
                Date date_emprunt = AllUtils.toJavaDate(result.getString("date_emprunt"));
                Date date_retour = null;
                if (result.getString("date_retour")!=null)
                    date_retour = AllUtils.toJavaDate(result.getString("date_retour"));
                Responsable resp = new Responsable();
                resp.setMatr(num_resp);
                Emprunt emp = new Emprunt();                
                emp.setDateEmprunt(date_emprunt);
                emp.setDateRetour(date_retour);
                emp.setId(1);
                emp.setResponsable(resp);
                if (liste.add(emp)){
                    System.out.println("Emprunt recupere");
                }else{
                    System.out.println("Emprunt non recupere ");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(GestionEmpruntsBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }
    
    public static DefaultTableModel retournerDefaultTableModel(int etat){
        ArrayList<Emprunt> liste = new ArrayList();  
        DefaultTableModel table = new DefaultTableModel();
        ResultSet result;
        try {            
            Statement state = ConnectionBdd.conn.createStatement();
            if (etat==1)
                result = state.executeQuery("SELECT id, Responsable.matr, Responsable.nom, Responsable.prenom, date_emprunt, date_retour  FROM Emprunt, Responsable WHERE rendu=1 AND Responsable.matr = Emprunt.matr_resp");
            else if (etat==0)
                result = state.executeQuery("SELECT id, Responsable.matr, Responsable.nom, Responsable.prenom, date_emprunt, date_retour  FROM Emprunt, Responsable WHERE rendu=0 AND Responsable.matr = Emprunt.matr_resp");
            else
                result = state.executeQuery("SELECT id, Responsable.matr, Responsable.nom, Responsable.prenom, date_emprunt, date_retour  FROM Emprunt, Responsable WHERE Responsable.matr = Emprunt.matr_resp");
                
//            Emprunt emp = new Emprunt(num, resp, dateEmprunt, true)
            
//            while (result.next()){
//                int id = result.getInt("id");
//                String num_resp = result.getString("num_resp");
//                Date date_emprunt = AllUtils.toJavaDate(result.getString("date_emprunt"));
//                Date date_retour = null;
//                if (result.getString("date_retour")!=null)
//                    date_retour = AllUtils.toJavaDate(result.getString("date_retour"));
//                Responsable resp = new Responsable();
//                resp.setMatr(num_resp);
//                Emprunt emp = new Emprunt();                
//                emp.setDateEmprunt(date_emprunt);
//                emp.setDateRetour(date_retour);
//                emp.setId(id);
//                emp.setResponsable(resp);
//                if (liste.add(emp)){
//                    System.out.println("Emprunt recupere");
//                }else{
//                    System.out.println("Emprunt non recupere ");
//                }
//            }
            
            table = AllUtils.buildTableModel(result);
        } catch (SQLException ex) {
            Logger.getLogger(GestionEmpruntsBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return table;
    }
    
    public static ArrayList<Integer> getIdMaterielsOfEmp(int id){
        ArrayList<Integer> listeId = new ArrayList();
        try {
            Emprunt emp = null;
            Statement state = ConnectionBdd.conn.createStatement();
            ResultSet result = state.executeQuery("SELECT id_materiel FROM Emprunt_Materiel WHERE id_emp="+id);
            while(result.next()){
                listeId.add(result.getInt("id_materiel"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(GestionEmpruntsBdd.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listeId;
    }
}

