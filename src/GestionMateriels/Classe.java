package GestionMateriels;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author micebishop
 */
public class Classe {
    private String nom;
    private Responsable responsable;

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the responsable
     */
    public Responsable getResponsable() {
        return responsable;
    }

    /**
     * @param responsable the responsable to set
     */
    public void setResponsable(Responsable responsable) {
        this.responsable = responsable;
    }
    
    public Classe(){
        
    }
    
    public Classe(String nom, Responsable responsable){
        this.nom = nom;
        this.responsable = responsable;
    }
    
    public Classe(String nom){
        this.nom = nom;
    }
    
}
