/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GestionMateriels;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author micebishop
 */
public class AllUtils {
    
    /**
     *
     * @param date
     * @return String
     * Prends un objet de type Date et retourne la date en chaine de caracteres
     */
    public static String toSqlDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
    
    /**
     *
     * @param date
     * @return Date
     * Prends une date en chaine de caracteres et retourne un objet de type de Date
     */
    public static Date toJavaDate(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;
        try {
            d = sdf.parse(date);
        } catch (ParseException ex) {
            Logger.getLogger(AllUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return d;
    }
    
    public static String genererMatr(){
        
        Calendar c = Calendar.getInstance();
        Date d = new Date();
        c.setTime(d);
        int annee = c.get(Calendar.YEAR);
        
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 3; i++) {
            char ch = chars[random.nextInt(chars.length)];
            sb.append(ch);
        }
        
        char[] numbers = "0123456789".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 2; i++) {
            char ch1 = numbers[random1.nextInt(numbers.length)];
            sb1.append(ch1);
        }
        
        String matr = String.valueOf(annee) + sb1.toString() + sb.toString();
        
        return matr;
    }
    
    public static String cropId(String label){
        int i;
        for (i=0; i<label.length(); i++){
           if (label.charAt(i) == '-'){
               break;
           }
        }
        String idCroped = label.substring(0, i);
        return idCroped;
    }
    
    public static DefaultTableModel buildTableModel(ResultSet rs)
        throws SQLException {

    ResultSetMetaData metaData = rs.getMetaData();

    // names of columns
    Vector<String> columnNames = new Vector<String>();
    int columnCount = metaData.getColumnCount();
    for (int column = 1; column <= columnCount; column++) {
        columnNames.add(metaData.getColumnName(column));
    }

    // data of the table
    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
    while (rs.next()) {
        Vector<Object> vector = new Vector<Object>();
        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
            
            System.out.println(rs.getObject(columnIndex));
            vector.add(rs.getObject(columnIndex));
        }
        data.add(vector);
    }

    return new DefaultTableModel(data, columnNames);

}    
    
}
