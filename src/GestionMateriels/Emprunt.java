package GestionMateriels;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author micebishop
 */
public class Emprunt {
    private int id;
    private Responsable responsable;
    private ArrayList<Materiel>  listeMateriel;
    private Date dateEmprunt; 
    private Date dateRetour; // Specifie uniquement en cas de retour ... nulle sinon
    private boolean rendu; // Vrai si rendu, faux sinon 
    private String motif;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the responsable
     */
    public Responsable getResponsable() {
        return responsable;
    }

    /**
     * @param responsable the responsable to set
     */
    public void setResponsable(Responsable responsable) {
        this.responsable = responsable;
    }

    /**
     * @return the dateEmprunt
     */
    public Date getDateEmprunt() {
        return dateEmprunt;
    }

    /**
     * @param dateEmprunt the dateEmprunt to set
     */
    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }

    /**
     * @return the dateRetour
     */
    public Date getDateRetour() {
        return dateRetour;
    }

    /**
     * @param dateRetour the dateRetour to set
     */
    public void setDateRetour(Date dateRetour) {
        this.dateRetour = dateRetour;
    }

    /**
     * @return the rendu
     */
    public boolean isRendu() {
        return rendu;
    }

    /**
     * @param rendu the rendu to set
     */
    public void setRendu(boolean rendu) {
        this.rendu = rendu;
    }
    
    public Emprunt (Responsable resp, String motif){
        this.responsable = resp;
        this.dateEmprunt = new Date();
        this.rendu = false;
        this.motif = motif;
    }
    
    public Emprunt(){
        
    }

    /**
     * @return the motif
     */
    public String getMotif() {
        return motif;
    }

    /**
     * @param motif the motif to set
     */
    public void setMotif(String motif) {
        this.motif = motif;
    }

    /**
     * @return the listeMateriel
     */
    public ArrayList<Materiel> getListeMateriel() {
        return listeMateriel;
    }

    /**
     * @param listeMateriel the listeMateriel to set
     */
    public void setListeMateriel(ArrayList<Materiel> listeMateriel) {
        this.listeMateriel = listeMateriel;
    }
            
    
}
