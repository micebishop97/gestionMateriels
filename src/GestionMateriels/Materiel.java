package GestionMateriels;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Date;

/**
 *
 * @author micebishop
 */
public class Materiel {
    private int id;
    private TypeMateriel type;
    private String libelle;
    private Date dateLivraison;
    private EtatMateriel etat;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the type
     */
    public TypeMateriel getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(TypeMateriel type) {
        this.type = type;
    }

    /**
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param libelle the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return the dateLivraison
     */
    public Date getDateLivraison() {
        return dateLivraison;
    }

    /**
     * @param dateLivraison the dateLivraison to set
     */
    public void setDateLivraison(Date dateLivraison) {
        this.dateLivraison = dateLivraison;
    }
    
        /**
     * @return the etat
     */
    public EtatMateriel getEtat() {
        return etat;
    }

    /**
     * @param etat the etat to set
     */
    public void setEtat(EtatMateriel etat) {
        this.etat = etat;
    }

    
    public Materiel(int id, TypeMateriel type, String libelle, Date date_livraison, EtatMateriel etat){
        this.id = id;
        this.type = type;
        this.libelle = libelle;
        this.dateLivraison = date_livraison;
        this.etat = etat;
    }
    
    public Materiel(TypeMateriel type, String libelle){
        this.type = type;
        this.libelle = libelle;
        this.dateLivraison = new Date();
        this.etat = EtatMateriel.Disponible;
    }
    
    public Materiel(){
        
    }


    
}
